#include "eml_safety_enforcer/safety_check_ros_interface.h"
#include "emergency_maneuver_library/multirotor_emergency_maneuver_library.h"
#include "pose_graph_representation/pose_graph_representation.h"

namespace ca_ri=ca::representation_interface;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "safety_check");
  ros::NodeHandle n("~");

  ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>("pose_graph_representation", 1000);
  ros::Rate loop_rate(10);
  int count = 0;
  visualization_msgs::MarkerArray ma;
  visualization_msgs::Marker m;


  ca_ri::PoseGraphRepresentation  pose_graph_rep;
  pose_graph_rep.SetParams(1,1.5,M_PI/3,15.0,"/world");
  pose_graph_rep.AddCylinders(Eigen::Vector3d(0,0,0));
  pose_graph_rep.AddCylinders(Eigen::Vector3d(10,0,0));

  pose_graph_rep.AddSensingPoint(Eigen::Vector3d(-7,0,-0.3),0.);
  pose_graph_rep.AddSensingPoint(Eigen::Vector3d(5,-0.3,-0.3),0.);

  double value;
  pose_graph_rep.GetValue(Eigen::Vector3d(5.,0,-0.3),value);
  ROS_INFO_STREAM("Value::"<<value);

  ma = pose_graph_rep.GetCylinder(0);
  m = pose_graph_rep.GetSectors(0);
  ma.markers.push_back(m);

  ca::eml::MultiRotorEmergencyManeuverLibrary hl_em;
  hl_em.setParams(n);

  ca::SafetyCheckBase safetycheck;
  safetycheck.setAbortPathLibrary(&hl_em);
  safetycheck.setRepresentationInterface(&pose_graph_rep);
  safetycheck.setVelocityResolution(0.5);
  safetycheck.setConsiderOccupied(0.9);
  safetycheck.setConsiderUnknown(0.1);
  safetycheck.setUnknownFractionAllowed(0.01);
  safetycheck.setMinReccomendedSpeed(0.);
    // TODO set all the parameters here

  ca::SafetyCheckROSInterface sfcr;
  sfcr.initialize(n,&safetycheck);

  double safeVelocity = 0;

  ca::eml::State q_state;
  q_state.frame = "/world";
  q_state.x=5.; q_state.y=0.; q_state.z=-0.3;
  q_state.roll=0.; q_state.pitch=0.; q_state.yaw=0.;
  q_state.v_x =-10.; q_state.v_y=0.; q_state.v_z=0.;
  q_state.v_roll = 0; q_state.v_pitch=0.; q_state.v_yaw=0.;

  std::vector<ca::eml::Trajectory> trajectoryVector =
          safetycheck.abortLibrary->getEmergencyManeuverLibraryState(q_state);
  std::vector<ca::eml::Trajectory> safeTrajectoryVector = safetycheck.returnSafeVelocity(q_state,safeVelocity);

  ROS_ERROR_STREAM("Trajectory Vector::"<<trajectoryVector.size());
  ROS_ERROR_STREAM("SafeVelocity::"<<safeVelocity<<"m/s");

  safetycheck.generateMarkersSafeUnSafeTrajectories(ma,&safeTrajectoryVector,&trajectoryVector);

  while(ros::ok())
  {
    sfcr.process();
    loop_rate.sleep();
    count++;
    marker_pub.publish(ma);
    ros::spinOnce();
  }
  return 0;
  
}
